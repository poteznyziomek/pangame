cmake_minimum_required(VERSION 3.24)
project(PanGame)

set(CMAKE_CXX_STANDARD 20)

add_executable(PanGame main.cpp
        gameElements/Ball.cpp
        gameElements/Ball.h
        gameElements/Board.cpp
        gameElements/Board.h
        Game.cpp
        Game.cpp
        gameElements/Player.cpp
        gameElements/Player.h
        gameElements/PlayerName.cpp
        gameElements/PlayerName.h
        gameElements/Goal.h)

include_directories(/usr/include c:/SFML/include)

set(SFML_ROOT c:/SFML)
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake_modules")
find_package(SFML REQUIRED system window graphics network audio)
if (SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(PanGame ${SFML_LIBRARIES} ${SFML_DEPENDENCIES})
endif()

