#ifndef PANGAME_GAME_CPP
#define PANGAME_GAME_CPP

#include <cmath>
#include "Game.h"

Game::Game() : gameBoard(Board()) {
    players[0] = new Player("Player 1", PlayerNr::PLAYER_ONE);
    players[1] = new Player("Player 2", PlayerNr::PLAYER_TWO);
    currentPlayer = players[0];
    currentPlayer->PlayerTurnStart();
    m_isEnd = false;

    if (ttexture.loadFromFile("../background.jpg")) {
        ssprite.setTexture(ttexture);
    }
    ssprite.setTextureRect(sf::IntRect(94, 133, 750, 800));
}

void Game::draw(sf::RenderWindow& mWindow) {
    mWindow.draw(ssprite);
    gameBoard.drawBoard(&mWindow);
    drawPlayers(&mWindow);
    mWindow.display();
}

void Game::hoverPoint(const sf::Vector2i mousePos) {
    gameBoard.toggleHoverPoint();
    sf::Vector2f point = getPointUnderMouse(mousePos);
    if (point.x == 0 || point.y == 0)
        return;
    gameBoard.toggleHoverPoint(point);
}

void Game::move(const sf::Vector2i mousePos) {
    sf::Vector2f point = getPointUnderMouse(mousePos);
    if (point.x == 0 || point.y == 0)
        return;
    const bool isPlayerShouldNotEndTurn = gameBoard.isBounce(point);
    m_isEnd = gameBoard.moveBall(point, currentPlayer->m_nr);
    if(m_isEnd)
        isOwnGoal();
    if(!m_isEnd)
        m_isEnd = !isAvailableMoves();
    if (!isPlayerShouldNotEndTurn && !m_isEnd) {
        currentPlayer->PlayerTurnEnd();
        changePlayer();
        currentPlayer->PlayerTurnStart();
    }
}

sf::Vector2f Game::getPointUnderMouse(const sf::Vector2i mousePos) {
    const sf::Vector2f ballPosition = gameBoard.getBallPosition();
    const bool isBallOnTheEdge = gameBoard.isBallOnTheEdge();
    const float up = ballPosition.y - DIST_B_P;
    const float down = ballPosition.y + DIST_B_P;
    const float left = ballPosition.x - DIST_B_P;
    const float right = ballPosition.x + DIST_B_P;

    for (unsigned x = 0; x < B_S_X; x++) {
        for (unsigned y = 0; y < B_S_Y; y++) {
            sf::Vector2f tempPointPos = gameBoard.getPointPosition(x, y);
            if (tempPointPos.x == 0 && tempPointPos.y == 0) continue;
            if (tempPointPos == ballPosition) continue;
            bool isInSquare = (tempPointPos.x >= left && tempPointPos.x <= right) && (tempPointPos.y >= up && tempPointPos.y <= down);
            bool isNotEdgeBetweenBallAndPoint = !(isBallOnTheEdge & gameBoard.isPointOnTheEdge(x, y));
            if (isInSquare && isNotEdgeBetweenBallAndPoint
                && !isAnyLineBetweenPoints(ballPosition, tempPointPos)) {
                if (std::pow((mousePos.x - tempPointPos.x), 2) + std::pow((mousePos.y - tempPointPos.y), 2)<= std::pow(H_P_RAD, 2)) {
                    return tempPointPos;
                }
            }
        }
    }
    for (unsigned i = 0; i < 6; i++) {
        sf::Vector2f tempGatePos = gameBoard.getGatePosition(i);
        if ((tempGatePos.x >= left && tempGatePos.x <= right)
            && (tempGatePos.y >= up && tempGatePos.y <= down)) {
            if (std::pow((mousePos.x - tempGatePos.x), 2) + std::pow((mousePos.y - tempGatePos.y), 2) <= std::pow(H_P_RAD, 2)) {
                return tempGatePos;
            }
        }
    }
    return sf::Vector2f(0, 0);
}

bool Game::isAnyLineBetweenPoints(const sf::Vector2f ballPos, const sf::Vector2f pointPos) {
    if (ballPos.x == pointPos.x) {
        if (ballPos.y > pointPos.y) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::TOP)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::DOWN));
        }
        else if (ballPos.y < pointPos.y) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::DOWN)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::TOP));
        }
    }
    else if (ballPos.y == pointPos.y) {
        if (ballPos.x > pointPos.x) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::LEFT)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::RIGHT));
        }
        else if (ballPos.x < pointPos.x) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::RIGHT)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::LEFT));
        }
    }
    else if (ballPos.x > pointPos.x) {
        if (ballPos.y > pointPos.y) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::TOP_LEFT)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::DOWN_RIGHT));
        }
        else if (ballPos.y < pointPos.y) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::DOWN_LEFT)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::TOP_RIGHT));
        }
    }
    else if (ballPos.x < pointPos.x) {
        if (ballPos.y > pointPos.y) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::TOP_RIGHT)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::DOWN_LEFT));
        }
        else if (ballPos.y < pointPos.y) {
            return (gameBoard.isLineOnPoint(ballPos.x, ballPos.y, Directions::DOWN_RIGHT)
                    && gameBoard.isLineOnPoint(pointPos.x, pointPos.y, Directions::TOP_LEFT));
        }
    }
    return false;
}

void Game::drawPlayers(sf::RenderWindow* mWindow) {
    for (Player* player : players) {
        mWindow->draw(player->getText());
    }
}

bool Game::isEnd() {
    return m_isEnd;
}

std::string Game::getWinner() {
    return currentPlayer->getName() + " won!";
}

void Game::isOwnGoal() {
    if (gameBoard.whoseGate() == currentPlayer->m_nr) {
        changePlayer();
    }
}

bool Game::isAvailableMoves() {
    const sf::Vector2f ballPosition = gameBoard.getBallPosition();
    const bool isBallOnTheEdge = gameBoard.isBallOnTheEdge();
    const unsigned up = ballPosition.y - DIST_B_P;
    const unsigned down = ballPosition.y + DIST_B_P;
    const unsigned left = ballPosition.x - DIST_B_P;
    const unsigned right = ballPosition.x + DIST_B_P;
    for (unsigned x = 0; x < B_S_X; x++) {
        for (unsigned y = 0; y < B_S_Y; y++) {
            sf::Vector2f tempPointPos = gameBoard.getPointPosition(x, y);
            if (tempPointPos.x == 0 && tempPointPos.y == 0) continue;
            if (tempPointPos == ballPosition) continue;
            bool isInSquare = (tempPointPos.x >= left && tempPointPos.x <= right) && (tempPointPos.y >= up && tempPointPos.y <= down);
            bool isNotEdgeBetweenBallAndPoint = !(isBallOnTheEdge & gameBoard.isPointOnTheEdge(x, y));
            if (isInSquare && isNotEdgeBetweenBallAndPoint && !isAnyLineBetweenPoints(ballPosition, tempPointPos)) {
                return true;
            }
        }
    }
    for (unsigned i = 0; i < 6; i++) {
        sf::Vector2f tempGatePos = gameBoard.getGatePosition(i);
        if ((tempGatePos.x >= left && tempGatePos.x <= right)
            && (tempGatePos.y >= up && tempGatePos.y <= down)) {
            return true;
        }
    }
    changePlayer();
    return false;
}

void Game::changePlayer() {
    if (currentPlayer->m_nr == PLAYER_ONE)
        currentPlayer = players[1];
    else
        currentPlayer = players[0];
}


#endif //PANGAME_GAME_CPP
