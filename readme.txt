Piłkarzyki na kartce w komputrze

Gra dla 2 graczy, każdy z graczy turowo wykonują swoje posunięcia.
Celem gry jest wbicie bramki przeciwnikowi.
Początek gry jest na środku planszy, z każdego punktu można zrobić jeden ruch w górę, dół, lewo, prawo lub po skosach.
Można odbijać się od ściań oraz punktów, które mają jakiekolwiek "połączenie".

Gra kończy się w przypadku bramki lub zablokowania (brak dostępnych ruchów)

Nie można ruszać się po narysowanych już liniach!!

Sterowanie:

LPM (lewy przycisk myszy) na dostępny punkt z miejsca gdzie aktualnie znajduje się "piłka".