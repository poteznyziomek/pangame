#include "Board.h"

Board::Board() {
    initPoints();
    initHoverPoint();
    initFrame();
    initBall();
    initLines();
    initGates();
}

void Board::drawBoard(sf::RenderWindow* hWindow) {
    drawFrame(hWindow);
    drawPoints(hWindow);
    drawGoals(hWindow);
    hWindow->draw(lines);
    if (hoverPoint.getPosition().x != -20)
        hWindow->draw(hoverPoint);
}

sf::Vector2f Board::getPointPosition(const unsigned x, const unsigned y) {
    if (B_S_X < x || B_S_Y < y) return {};
    return points[x][y].getPosition();
}

bool Board::moveBall(sf::Vector2f newPositionOfBall, PlayerNr player) {
    Point* newBall = getPoint(newPositionOfBall);
    if (newBall->getPosition() != sf::Vector2f(0, 0)) {
        ball.setNewBall(newBall);
        if(player == PLAYER_ONE) {
            lines.append(sf::Vertex(newBall->getPosition(), sf::Color(33, 62, 118, 255)));
        }
        else {
            lines.append(sf::Vertex(newBall->getPosition(), sf::Color(255, 0, 0, 255)));
        }
        return false;
    }
    else {
        Goal* newBall = getGoal(newPositionOfBall);
        ball.setGoal((Point*)newBall);

        lines.append(sf::Vertex(newBall->getPosition(), sf::Color(33, 62, 118, 255))
        );
        return true;
    }
}

bool Board::isLineOnPoint(const float x, const float y, const uint8_t direction) {
    return getPoint(sf::Vector2f(x, y))->isLine(direction);
}

void Board::toggleHoverPoint(const sf::Vector2f position) {
    hoverPoint.setPosition(position);
}

sf::Vector2f Board::getBallPosition() {
    return ball.getPosition();
}

bool Board::isBounce(const sf::Vector2f pointPosition) {
    Point* point = getPoint(pointPosition);
    return (point->isAnyConnections() | point->isEdge());
}

sf::Vector2f Board::getGatePosition(const unsigned iterator) {
    return goals[iterator].getPosition();
}

bool Board::isBallOnTheEdge() {
    return ball.isOnTheEdge();
}

bool Board::isPointOnTheEdge(const unsigned x, const unsigned y) {
    return points[x][y].isEdge();
}

PlayerNr Board::whoseGate() {
    for (auto & m_gate : goals) {
        if (m_gate.getPosition() == ball.getPosition())
            return m_gate.getPlayer();
    }
    return PlayerNr::NONE;
}


void Board::initPoints() {
    sf::CircleShape* tempCirc;
    for (int y = 0; y < B_S_Y; ++y) {
        for (int x = 0; x < B_S_X; x++) {
            tempCirc = &points[x][y];
            if (x == (B_S_X - 1) / 2)
                points[x][y] = Point(P_RAD, false);
            else if (x == 0 || y == 0 || x == (B_S_X - 1) || y == (B_S_Y - 1))
                points[x][y] = Point(P_RAD, true);
            else
                points[x][y] = Point(P_RAD, false);
            tempCirc->setFillColor(sf::Color(33, 62, 118, 255));
            tempCirc->setOrigin(P_RAD, P_RAD);
            tempCirc->setPosition(
                    (static_cast<float>(x) * DIST_B_P) + M,
                    (static_cast<float>(y) * DIST_B_P) + M
            );
        }
    }
}

void Board::initFrame() {
    const float GATE_LEFT_CORNER_POINT_X = (((B_S_X - 1) / 2) - 1) * DIST_B_P + M;
    const float GATE_RIGHT_CORNER_POINT_X = GATE_LEFT_CORNER_POINT_X + DIST_B_P * G_S;
    const float RIGHT_CORNER_X = M + DIST_B_P * (B_S_X - 1);
    const float UPPER_ENDLINE_Y = M;
    const float UPPER_GATELINE_Y = (M - DIST_B_P);
    const float BOTTOM_ENDlINE_Y = M + DIST_B_P * (B_S_Y - 1);
    const float BOTTOM_GATELINE_Y = M + DIST_B_P * B_S_Y;

    frame = sf::ConvexShape(12);
    frame.setPoint(0, sf::Vector2f(M, UPPER_ENDLINE_Y));
    frame.setPoint(1, sf::Vector2f(GATE_LEFT_CORNER_POINT_X, UPPER_ENDLINE_Y));
    frame.setPoint(2, sf::Vector2f(GATE_LEFT_CORNER_POINT_X, UPPER_GATELINE_Y));
    frame.setPoint(3, sf::Vector2f(GATE_RIGHT_CORNER_POINT_X, UPPER_GATELINE_Y));
    frame.setPoint(4, sf::Vector2f(GATE_RIGHT_CORNER_POINT_X, UPPER_ENDLINE_Y));
    frame.setPoint(5, sf::Vector2f(RIGHT_CORNER_X, UPPER_ENDLINE_Y));
    frame.setPoint(6, sf::Vector2f(RIGHT_CORNER_X, BOTTOM_ENDlINE_Y));
    frame.setPoint(7, sf::Vector2f(GATE_RIGHT_CORNER_POINT_X, BOTTOM_ENDlINE_Y));
    frame.setPoint(8, sf::Vector2f(GATE_RIGHT_CORNER_POINT_X, BOTTOM_GATELINE_Y));
    frame.setPoint(9, sf::Vector2f(GATE_LEFT_CORNER_POINT_X, BOTTOM_GATELINE_Y));
    frame.setPoint(10, sf::Vector2f(GATE_LEFT_CORNER_POINT_X, BOTTOM_ENDlINE_Y));
    frame.setPoint(11, sf::Vector2f(M, BOTTOM_ENDlINE_Y));
}

void Board::initHoverPoint() {
    hoverPoint = sf::CircleShape(P_RAD * 2);
    hoverPoint.setFillColor(sf::Color::Transparent);
    hoverPoint.setOutlineColor(sf::Color(33, 62, 118));
    hoverPoint.setOutlineThickness(2);
    hoverPoint.setOrigin(P_RAD * 2, P_RAD * 2);
    toggleHoverPoint();
}

void Board::initBall() {
    ball = Ball(&points[(B_S_X - 1) / 2][(B_S_Y - 1) / 2]);
}

void Board::initLines() {
    lines = sf::VertexArray(sf::PrimitiveType::LineStrip);
    lines.append(sf::Vertex(ball.getPosition()));
    lines[0].color = sf::Color(33, 62, 118, 255);
}

void Board::initGates() {
    const float GATE_LEFT_CORNER_POINT_X = (((B_S_X - 1) / 2) - 1) * DIST_B_P + M;
    const float BOTTOM_ENDlINE_Y = M + DIST_B_P * (B_S_Y - 1);
    for (unsigned i = 0; i < 3; i++) {
        goals[i] = Goal(P_RAD, PlayerNr::PLAYER_ONE);
        goals[i].setOrigin(P_RAD, P_RAD);
        goals[i].setFillColor(sf::Color(33, 62, 118, 255));
        goals[i].setPosition(GATE_LEFT_CORNER_POINT_X + (DIST_B_P * i), M - DIST_B_P
        );
    }
    for (unsigned i = 3; i < 6; i++) {
        goals[i] = Goal(P_RAD, PlayerNr::PLAYER_TWO);
        goals[i].setOrigin(P_RAD, P_RAD);
        goals[i].setFillColor(sf::Color(255, 0, 0, 255));
        goals[i].setPosition(GATE_LEFT_CORNER_POINT_X + (DIST_B_P * (i - 3)), M + DIST_B_P * B_S_Y
        );
    }
}

void Board::drawPoints(sf::RenderWindow* hWindow) {
    for (unsigned y = 0; y < B_S_Y; ++y) {
        for (unsigned x = 0; x < B_S_X; x++) {
            hWindow->draw(points[x][y]);
        }
    }
}

void Board::drawFrame(sf::RenderWindow* hWindow) {
    hWindow->draw(frame);
    frame.setOutlineThickness(4);
    frame.setOutlineColor(sf::Color(18, 32, 59, 255));
    frame.setFillColor(sf::Color(209, 235, 255, 255));
}

void Board::drawGoals(sf::RenderWindow* hWindow) {
    for (const auto & m_gate : goals) {
        hWindow->draw(m_gate);
    }
}

Point* Board::getPoint(const sf::Vector2f pointPos) {
    for (unsigned y = 0; y < B_S_Y; ++y) {
        for (auto & m_point : points) {
            if (m_point[y].getPosition() == pointPos)
                return &m_point[y];
        }
    }
    return new Point();
}

Goal* Board::getGoal(sf::Vector2f gatePos) {
    for (auto & m_gate : goals) {
        if (m_gate.getPosition() == gatePos)
            return &m_gate;
    }

    return new Goal();
}
