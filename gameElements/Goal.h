#ifndef PANGAME_GOAL_H
#define PANGAME_GOAL_H

#include <SFML/Graphics/CircleShape.hpp>

#include "Player.h"

class Goal : public sf::CircleShape {
private:
    PlayerNr m_player;

public:
    Goal() {}
    Goal(const float point, const PlayerNr player)
            : sf::CircleShape(point), m_player(player) {};

    PlayerNr getPlayer() {
        return m_player;
    }
};


#endif //PANGAME_GOAL_H
