#ifndef PANGAME_BOARD_H
#define PANGAME_BOARD_H

#pragma once

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "Ball.h"
#include "Goal.h"

#define B_S_X 9
#define B_S_Y 11
#define P_RAD 3
#define H_P_RAD (P_RAD * 4)
#define DIST_B_P 51
#define M 150
#define G_S 2

class Board {
private:
    Point points[B_S_X][B_S_Y];
    Goal goals[(G_S + 1) * 2];
    sf::CircleShape hoverPoint;
    sf::ConvexShape frame;
    Ball ball;
    sf::VertexArray lines;

    void initPoints();
    void initFrame();
    void initHoverPoint();
    void initBall();
    void initLines();
    void initGates();

    void drawPoints(sf::RenderWindow* hWindow);
    void drawFrame(sf::RenderWindow* hWindow);
    void drawGoals(sf::RenderWindow* hWindow);

    Point* getPoint(sf::Vector2f pointPos);
    Goal* getGoal(sf::Vector2f gatePos);

public:
    Board();
    void drawBoard(sf::RenderWindow* hWindow);
    sf::Vector2f getPointPosition(unsigned x, unsigned y);
    sf::Vector2f getGatePosition(unsigned iterator);
    bool moveBall(sf::Vector2f newPositionOfBall, PlayerNr player);
    bool isLineOnPoint(float x, float y, uint8_t direction);
    void toggleHoverPoint(sf::Vector2f position = sf::Vector2f(-20, -20));
    sf::Vector2f getBallPosition();
    bool isBounce(sf::Vector2f pointPosition);
    bool isBallOnTheEdge();
    bool isPointOnTheEdge(unsigned x, unsigned y);
    PlayerNr whoseGate();
};


#endif //PANGAME_BOARD_H
