#include "Player.h"
#include "Board.h"

Player::Player(const std::string name, const PlayerNr nr): m_name(PlayerName(name)), m_nr(nr) {
    if (nr == PlayerNr::PLAYER_ONE) {
        const float y = M - DIST_B_P * 2;
        const float x = M + ((DIST_B_P * B_S_X) / 2) + DIST_B_P / 2;
        m_name.setPosition(x, y);
    }

    else if (nr == PlayerNr::PLAYER_TWO) {
        const float y = M + (DIST_B_P * B_S_Y) + DIST_B_P / 2;
        const float x = M + ((DIST_B_P * B_S_X) / 2) + DIST_B_P / 2;
        m_name.setPosition(x, y);
    }
}

sf::Text Player::getText() {
    return m_name.getText();
}

void Player::PlayerTurnStart() {
    m_name.enableUnderline();
}

void Player::PlayerTurnEnd() {
    m_name.disableUnderline();
}

std::string Player::getName() {
    return m_name.getName();
}