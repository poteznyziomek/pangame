#ifndef PANGAME_PLAYERNAME_H
#define PANGAME_PLAYERNAME_H

#pragma once
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

class PlayerName {
private:
    sf::Font m_font;
    sf::Text m_nameToDisplay;
    std::string m_name;

public:
    PlayerName(std::string name = "NoName");
    sf::Text getText();
    void setPosition(float x, float y);
    void enableUnderline();
    void disableUnderline();
    std::string getName();
};

#endif //PANGAME_PLAYERNAME_H
