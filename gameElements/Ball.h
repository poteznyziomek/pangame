#ifndef PANGAME_BALL_H
#define PANGAME_BALL_H

#pragma once
#include <SFML/Graphics/CircleShape.hpp>

class Point : public sf::CircleShape {
private:
    uint8_t m_lines = 0b00000000;
    bool m_isEdge;
public:
    Point() {}
    Point(const float point,const bool isEdge) : sf::CircleShape(point), m_isEdge(isEdge) {};
    void newLine(const uint8_t direction) {
        m_lines |= direction;
    }
    bool isLine(const uint8_t direction) const {
        return (m_lines & direction);
    }
    bool isAnyConnections() const {
        return m_lines > 0;
    }
    bool isEdge() const {
        return m_isEdge;
    }
};

enum Directions {
    TOP = 0b00000001,
    TOP_RIGHT = 0b00000010,
    RIGHT = 0b00000100,
    DOWN_RIGHT = 0b00001000,
    DOWN = 0b00010000,
    DOWN_LEFT = 0b00100000,
    LEFT = 0b01000000,
    TOP_LEFT = 0b10000000
};

class Ball {
private:
    Point* m_point;
public:
    Ball() {}
    explicit Ball(Point* point) : m_point(point) {}
    sf::Vector2f getPosition();
    [[maybe_unused]] bool isLine(uint8_t direction);
    void setNewBall(Point* newBall);
    bool isOnTheEdge();
    void setGoal(Point* gate);
};


#endif //PANGAME_BALL_H
