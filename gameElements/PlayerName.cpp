#include "PlayerName.h"

PlayerName::PlayerName(const std::string name) : m_name(name) {
    if (m_font.loadFromFile("../Consolas.ttf")) {
        m_nameToDisplay.setFont(m_font);
        m_nameToDisplay.setString(m_name);
        m_nameToDisplay.setCharacterSize(24);
        m_nameToDisplay.setFillColor(sf::Color(0, 0, 0));
        m_nameToDisplay.setOrigin((m_name.length() * 24) / 2, 0);
    }
}

sf::Text PlayerName::getText() {
    return m_nameToDisplay;
}

void PlayerName::setPosition(const float x, const float y) {
    m_nameToDisplay.setPosition(x, y);
}

void PlayerName::enableUnderline() {
    m_nameToDisplay.setStyle(sf::Text::Underlined);
}

void PlayerName::disableUnderline() {
    m_nameToDisplay.setStyle(sf::Text::Regular);
}

std::string PlayerName::getName() {
    return m_name;
}