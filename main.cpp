#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "Game.h"

int main() {
    sf::RenderWindow mainWindow(sf::VideoMode(750, 800), "Paper-Soccer", sf::Style::Titlebar | sf::Style::Close);

    Game game = Game();
    while (mainWindow.isOpen()) {
        sf::Event event{};
        while (mainWindow.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                mainWindow.close();
            if (event.key.code == sf::Mouse::Left) {
                game.move(sf::Mouse::getPosition(mainWindow));
            }
        }
        game.hoverPoint(sf::Mouse::getPosition(mainWindow));
        game.draw(mainWindow);
        if (game.isEnd())
            break;
    }

    if (game.isEnd()) {
        std::string string = game.getWinner();

        const unsigned fontSize = 50;
        const unsigned x = string.length() * (fontSize - 20);

        sf::RenderWindow endWindow(sf::VideoMode(x, fontSize + 10), "End", sf::Style::None);

        sf::Font font = sf::Font();
        sf::Text text = sf::Text();
        if (font.loadFromFile("../Consolas.ttf")) {
            text.setFont(font);
            text.setString(string);
            text.setCharacterSize(fontSize);
            text.setFillColor(sf::Color::Black);
        }
        while (endWindow.isOpen()) {
            sf::Event event{};
            while (endWindow.pollEvent(event) || mainWindow.pollEvent(event)) {
                if (event.key.code == sf::Keyboard::Escape) {
                    endWindow.close();
                    mainWindow.close();
                }
            }
            endWindow.clear();
            endWindow.draw(text);
            endWindow.display();
        }
    }

    return 0;
}
