#ifndef PANGAME_GAME_H
#define PANGAME_GAME_H

#pragma once

#include <SFML/Graphics.hpp>
#include "gameElements/Board.h"
#include "gameElements/Player.h"

class Game {
private:
    Board gameBoard;
    Player* players[2];
    Player* currentPlayer;
    sf::Sprite ssprite;
    sf::Texture ttexture;
    bool m_isEnd;

    sf::Vector2f getPointUnderMouse(sf::Vector2i mousePos);
    bool isAnyLineBetweenPoints(sf::Vector2f ballPos, sf::Vector2f pointPos);
    void drawPlayers(sf::RenderWindow* mWindow);
    void isOwnGoal();
    bool isAvailableMoves();
    void changePlayer();

public:
    Game();

    void draw(sf::RenderWindow& mWindow);
    void hoverPoint(sf::Vector2i mousePos);
    void move(sf::Vector2i mousePos);
    bool isEnd();
    std::string getWinner();
};



#endif //PANGAME_GAME_H
